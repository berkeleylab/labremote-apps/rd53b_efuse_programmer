// std/stl
#include <getopt.h>

#include <experimental/filesystem>
#include <iostream>
#include <memory>
#include <sstream>
#include <string>
#include <thread>
#include <vector>
namespace fs = std::experimental::filesystem;

// e-fuse programmer
#include "EquipmentHelpers.h"
#include "FileUtils.h"
#include "RD53BHelpers.h"

// YARR
//#include "Rd53b.h"
//#include "ScanHelper.h"
//#include "Bookkeeper.h"
//#include "SpecController.h"

// labRemote
#include "EquipConf.h"
#include "IPowerSupply.h"
#include "Logger.h"
#include "PowerSupplyChannel.h"

struct option longopts_t[] = {{"controller", required_argument, NULL, 'r'},
                              {"connectivity", required_argument, NULL, 'c'},
                              {"efuse", required_argument, NULL, 'e'},
                              {"debug", no_argument, NULL, 'd'},
                              {"help", no_argument, NULL, 'h'},
                              {0, 0, 0, 0}};

void print_help(std::string program_name) {
    std::cout << "============================================================="
                 "=========="
              << std::endl;
    std::cout << " RD53B E-Fuse Programmer" << std::endl;
    std::cout << std::endl;
    std::cout << " Usage: " << program_name << " [OPTIONS]" << std::endl;
    std::cout << std::endl;
    std::cout << " Options:" << std::endl;
    std::cout << "   -r   YARR controller config JSON file [REQUIRED]"
              << std::endl;
    std::cout << "   -c   YARR connectivity config JSON file [REQUIRED]"
              << std::endl;
    std::cout << "   -e   The 32-bit value to write to the e-fuses [hex string "
                 "WITHOUT the leading 0x]"
              << std::endl;
    std::cout << "   -d   Increase verbosity" << std::endl;
    std::cout << "   -h   Print this help message" << std::endl;
    std::cout << std::endl;
    std::cout << " Notes:" << std::endl;
    std::cout << "   Do not provide the \"-e\" option if you just wish to read "
                 "the current"
              << std::endl;
    std::cout << "   E-Fuse data of the connected chip." << std::endl;
    std::cout << "============================================================="
                 "=========="
              << std::endl;
}

int main(int argc, char* argv[]) {
    std::string yarr_controller_config_filename = "";
    std::string yarr_connectivity_config_filename = "";
    std::string efuse_to_set_input = "";
    int c;
    while ((c = getopt_long(argc, argv, "r:c:dhe:", longopts_t, NULL)) != -1) {
        switch (c) {
            case 'r':
                yarr_controller_config_filename = optarg;
                break;
            case 'c':
                yarr_connectivity_config_filename = optarg;
                break;
            case 'e':
                efuse_to_set_input = optarg;
                break;
            case 'd':
                logIt::incrDebug();
                break;
            case 'h':
                print_help(argv[0]);
                return 0;
                break;
            case '?':
            default:
                logger(logERROR) << "Invalid command-line argument provided";
                return 1;
        }  // switch
    }      // while
    if (yarr_controller_config_filename == "") {
        logger(logERROR)
            << "ERROR: Did not provide YARR controller configuration file";
        return 1;
    }
    if (yarr_connectivity_config_filename == "") {
        logger(logERROR)
            << "ERROR: Did not provide YARR connectivity configuration file";
        return 1;
    }

    // check that the input files exist
    fs::path controller_config_path(yarr_controller_config_filename);
    if (!fs::exists(controller_config_path)) {
        logger(logERROR) << "Provided YARR controller config file (=\""
                         << yarr_controller_config_filename
                         << "\") does not exist!";
        return 1;
    }
    fs::path connectivity_config_path(yarr_connectivity_config_filename);
    if (!fs::exists(connectivity_config_path)) {
        logger(logERROR) << "Provided YARR connectivity config file (=\""
                         << yarr_connectivity_config_filename
                         << "\") does not exist!";
        return 1;
    }

    bool read_only = true;
    uint32_t efuse_to_set = 0x0;
    if (efuse_to_set_input != "") {
        read_only = false;
        try {
            efuse_to_set = std::stol(efuse_to_set_input, nullptr, 16);
            logger(logDEBUG)
                << "Writing E-Fuse data: 0x" << std::hex << efuse_to_set;
        } catch (std::exception& e) {
            logger(logERROR) << "Failed to parse provided E-fuse data input: "
                             << efuse_to_set_input;
            return 1;
        }
    }

    //
    // setup the front-end
    //
    namespace rh = rd53b::helpers;
    auto hw = rh::spec_init(yarr_controller_config_filename);
    auto fe = rh::rd53b_init(hw, yarr_connectivity_config_filename);

    // check the provided configuration
    if (fe->ServiceBlockEn.read() == 0) {
        logger(logERROR) << "RD53B register messages are not enabled "
                            "(ServiceBlockEn register is ==0)";
        return 1;
    }

    //
    // run the ToT memory clearing routine
    //
    rh::clear_tot_memories(hw, fe);
    std::this_thread::sleep_for(std::chrono::milliseconds(100));

    //
    // setup the controller
    //
    hw->setupMode();
    auto cfg = dynamic_cast<FrontEndCfg*>(fe.get());
    auto chipId = fe->getChipId();
    logger(logDEBUG) << "Loaded configuration for chip with ChipID = 0x"
                     << std::hex << chipId;

    // prepare for sending commands
    hw->setTrigEnable(0x0);
    hw->flushBuffer();  // clear out anything before doing the register readback
    hw->setCmdEnable(cfg->getTxChannel());
    hw->setRxEnable(cfg->getRxChannel());

    if (!read_only) {
        logger(logDEBUG) << "Writing e-fuses...";
        std::this_thread::sleep_for(std::chrono::milliseconds(1000));
        rh::write_efuses(fe, hw, efuse_to_set);
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
    } else {
        uint32_t efuse_data_readback = rh::read_efuses(fe, hw);
        std::stringstream ef;
        ef << "0x" << std::hex << efuse_data_readback;
        std::cout << ef.str() << std::endl;
        return 0;
    }

    return 0;
}
