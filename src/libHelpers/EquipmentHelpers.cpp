#include "EquipmentHelpers.h"

// labremote
#include "EquipConf.h"
#include "IPowerSupply.h"
#include "Logger.h"
#include "PowerSupplyChannel.h"

// std/stl
#include <algorithm>

namespace equipment {
namespace helpers {

std::shared_ptr<IPowerSupply> ps_init(std::string config, std::string ps_name) {
    logger(logDEBUG) << "Initializing power-supply \"" << ps_name << "\"...";
    std::shared_ptr<EquipConf> hw = std::make_shared<EquipConf>();
    hw->setHardwareConfig(config);
    return hw->getPowerSupply(ps_name);
}

std::shared_ptr<PowerSupplyChannel> ps_channel_init(
    std::shared_ptr<IPowerSupply> ps, std::string ps_name, unsigned channel) {
    logger(logDEBUG) << "Initializing power-supply channel " << channel;
    std::shared_ptr<PowerSupplyChannel> ps_channel =
        std::make_shared<PowerSupplyChannel>(ps_name + std::to_string(channel),
                                             ps, channel);
    if (ps_channel) ps_channel->turnOff();
    return ps_channel;
}

std::shared_ptr<PowerSupplyChannel> ps_channel(std::string config,
                                               std::string channel) {
    logger(logDEBUG) << "Initializing power-supply channel " << channel;
    std::shared_ptr<EquipConf> hw = std::make_shared<EquipConf>();
    hw->setHardwareConfig(config);
    return hw->getPowerSupplyChannel(channel);
}

void ps_turn_on(std::vector<std::shared_ptr<PowerSupplyChannel>> channels,
                float voltage, float current_protect) {
    std::for_each(channels.begin(), channels.end(),
                  [voltage, current_protect](
                      std::shared_ptr<PowerSupplyChannel> ps_channel) {
                      ps_channel->turnOff();
                      ps_channel->setCurrentProtect(current_protect);
                      ps_channel->setVoltageLevel(voltage);
                      ps_channel->turnOn();
                  });
}

void ps_turn_off(std::vector<std::shared_ptr<PowerSupplyChannel>> channels) {
    std::for_each(channels.begin(), channels.end(),
                  [](std::shared_ptr<PowerSupplyChannel> ps_channel) {
                      ps_channel->turnOff();
                  });
}

};  // namespace helpers
};  // namespace equipment
