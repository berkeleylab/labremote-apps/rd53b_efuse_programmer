#ifndef EQUIPMENT_HELPERS_H
#define EQUIPMENT_HELPERS_H

// labremote
class IPowerSupply;
class PowerSupplyChannel;

// std/stl
#include <memory>
#include <string>
#include <vector>

namespace equipment {
namespace helpers {

std::shared_ptr<IPowerSupply> ps_init(std::string config, std::string ps_name);
std::shared_ptr<PowerSupplyChannel> ps_channel(std::string config,
                                               std::string channel_name);
std::shared_ptr<PowerSupplyChannel> ps_channel_init(
    std::shared_ptr<IPowerSupply> ps, std::string ps_name, unsigned channel);
void ps_turn_on(std::vector<std::shared_ptr<PowerSupplyChannel>> channels,
                float voltage, float current_protect);
void ps_turn_off(std::vector<std::shared_ptr<PowerSupplyChannel>> channels);

};  // namespace helpers

};  // namespace equipment

#endif
