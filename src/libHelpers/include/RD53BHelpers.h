#ifndef RD53B_HELPERS_H
#define RD53B_HELPERS_H

// std/stl
#include <memory>  // shared_ptr
#include <string>
#include <tuple>

// json
#include <nlohmann/json.hpp>

// yarr
// class SpecController;
#include "SpecController.h"
// class Rd53b;
#include "Rd53b.h"

namespace rd53b {

namespace helpers {
std::unique_ptr<SpecController> spec_init(std::string config);
bool spec_init_trigger(std::unique_ptr<SpecController>& hw,
                       nlohmann::json trigger_config);
bool spec_trigger_loop(std::unique_ptr<SpecController>& hw);

std::unique_ptr<Rd53b> rd53b_init(std::unique_ptr<SpecController>& hw,
                                  std::string config);
bool rd53b_reset(std::unique_ptr<SpecController>& hw,
                 std::unique_ptr<Rd53b>& fe);

bool clear_tot_memories(std::unique_ptr<SpecController>& hw,
                        std::unique_ptr<Rd53b>& fe,
                        float pixel_fraction = 100.0);
bool disable_pixels(std::unique_ptr<Rd53b>& fe);
void set_core_columns(std::unique_ptr<Rd53b>& fe,
                      std::array<uint16_t, 4> cores);

void write_efuses(std::unique_ptr<Rd53b>& fe,
                  std::unique_ptr<SpecController>& hw, uint32_t efuse);
uint32_t read_efuses(std::unique_ptr<Rd53b>& fe,
                     std::unique_ptr<SpecController>& hw);
std::pair<uint32_t, uint32_t> decode_register(uint32_t higher, uint32_t lower);
uint32_t read_register(std::unique_ptr<Rd53b>& fe,
                       std::unique_ptr<SpecController>& hw,
                       uint32_t register_address);

};  // namespace helpers

};  // namespace rd53b

#endif
