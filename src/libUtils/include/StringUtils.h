#ifndef STRINGUTILS_H
#define STRINGUTILS_H

// std/stl
#include <algorithm>
#include <cctype>
#include <locale>
#include <string>
#include <vector>

namespace rd53b {
namespace utils {

std::string replace_substr(std::string input, std::string old_substring,
                           std::string new_substring);
std::vector<std::string> split(std::string s, std::string delim);

// trim from start (in place)
static inline void ltrim(std::string &s) {
    s.erase(s.begin(), std::find_if(s.begin(), s.end(),
                                    [](int ch) { return !std::isspace(ch); }));
}

// trim from end (in place)
static inline void rtrim(std::string &s) {
    s.erase(std::find_if(s.rbegin(), s.rend(),
                         [](int ch) { return !std::isspace(ch); })
                .base(),
            s.end());
}

// trim from both ends (in place)
static inline void trim(std::string &s) {
    ltrim(s);
    rtrim(s);
}

// trim from start (copying)
static inline std::string ltrim_copy(std::string s) {
    ltrim(s);
    return s;
}

// trim from end (copying)
static inline std::string rtrim_copy(std::string s) {
    rtrim(s);
    return s;
}

// trim from both ends (copying)
static inline std::string trim_copy(std::string s) {
    trim(s);
    return s;
}


};  // namespace utils
};  // namespace rd53b

#endif  // STRINGUTILS_H
