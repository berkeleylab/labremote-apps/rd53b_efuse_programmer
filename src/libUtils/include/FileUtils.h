#ifndef FILEUTILS_H
#define FILEUTILS_H

// std/std
#include <string>

namespace rd53b {
namespace utils {

std::string remove_substr(std::string input_str, std::string sub_str);
std::string dir_date_name(std::string base_dir_name, std::string suffix = "");
bool create_dir_symlink(std::string existing_dir, std::string sym_link_name);
std::string workbench_dir();
std::string default_data_dir(std::string program_name, std::string suffix = "");
std::string default_anamon_configfile();
std::string datasinks_configfile();

};  // namespace utils
};  // namespace rd53b

#endif
