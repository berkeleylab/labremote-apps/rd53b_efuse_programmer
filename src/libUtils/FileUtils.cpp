// std/stl
#include <ctime>
#include <experimental/filesystem>
#include <iomanip>  // put_time
#include <iostream>
#include <sstream>
namespace fs = std::experimental::filesystem;

// labemote
#include "Logger.h"

namespace rd53b {
namespace utils {

std::string remove_substr(std::string input_str, std::string sub_str) {
    // from
    // https://stackoverflow.com/questions/32435003/how-to-remove-all-substrings-from-a-string
    std::string out = input_str;
    std::string::size_type n = sub_str.length();
    for (std::string::size_type i = out.find(sub_str); i != std::string::npos;
         i = out.find(sub_str)) {
        out.erase(i, n);
    }
    return out;
}

std::string dir_date_name(std::string base_dir_name, std::string suffix) {
    auto t = std::time(nullptr);
    auto tm = *std::localtime(&t);
    fs::path base = base_dir_name;

    std::stringstream date;
    date << std::put_time(&tm, "%Y-%m-%d");

    std::stringstream tmp_new_dir;
    for (unsigned i = 0; i < 9999; i++) {
        tmp_new_dir.str("");
        tmp_new_dir << date.str();
        if (suffix != "") {
            tmp_new_dir << "_" << suffix;
        }
        tmp_new_dir << "_" << std::setfill('0') << std::setw(4) << i;
        fs::path check_out_dir = base / tmp_new_dir.str();
        if (!fs::exists(check_out_dir)) {
            return std::string(check_out_dir);
        }
    }  // i
    return "";
}

bool create_dir_symlink(std::string existing_dir, std::string sym_link_name) {
    fs::path existing = existing_dir;
    fs::path parent = existing.parent_path();
    fs::path sym = parent / sym_link_name;
    if (fs::exists(sym)) fs::remove(sym);
    std::stringstream sym_str;
    sym_str << "ln -s " << existing_dir << " " << std::string(sym);
    std::system(sym_str.str().c_str());
    if (!fs::exists(sym)) {
        logger(logWARNING) << "Failed to create symbolic link to directory (="
                           << existing_dir << ")";
        return false;
    }
    return true;
}

std::string workbench_dir() {
    std::string cwd = fs::current_path();
    fs::path p = cwd;
    while (true) {
        std::string tmp_name = p.parent_path().filename();
        if (tmp_name == "rd53b_workbench") {
            return p.parent_path();
        }
        p = p.parent_path();
    }
    return "";
}

std::string default_data_dir(std::string program_name, std::string suffix) {
    std::string wb_dir = workbench_dir();
    if (wb_dir == "") return fs::current_path();
    fs::path p_data(wb_dir);
    p_data /= "data";
    p_data /= fs::path(program_name);
    std::string date_dir = rd53b::utils::dir_date_name(p_data, suffix);
    fs::create_directories(date_dir);
    rd53b::utils::create_dir_symlink(date_dir, "last_output");
    return date_dir;
}

std::string default_anamon_configfile() {
    std::string wb_dir = workbench_dir();
    if (wb_dir == "") return "";
    fs::path p_config(wb_dir);
    p_config /= "rd53b_anamon";
    p_config /= "config";
    p_config /= "scc_anamon_config.json";
    if (!fs::exists(p_config)) {
        logger(logERROR) << "Default analog monitor SCC config (=\""
                         << std::string(p_config) << "\") could not be found";
        return "";
    }
    return std::string(p_config);
}

std::string datasinks_configfile() {
    std::string wb_dir = workbench_dir();
    if (wb_dir == "") return "";
    fs::path p_config(wb_dir);
    p_config /= "config";
    p_config /= "datasinks_config.json";
    if (!fs::exists(p_config)) {
        logger(logERROR) << "Datasinks config file (=\""
                         << std::string(p_config) << "\") could not be found";
        return "";
    }
    return std::string(p_config);
}

};  // namespace utils
};  // namespace rd53b
