#include "StringUtils.h"

namespace rd53b {
namespace utils {

std::string replace_substr(std::string in, std::string old_sub,
                           std::string new_sub) {
    std::string out = in;
    while (out.find(old_sub) != std::string::npos) {
        size_t idx = out.find(old_sub);
        out.replace(out.begin() + idx, out.begin() + idx + old_sub.length(),
                    new_sub);
    }
    return out;
}

std::vector<std::string> split(std::string s, std::string delim) {
    std::vector<std::string> out;
    if (s.find(delim) == std::string::npos) {
        out.push_back(rd53b::utils::trim_copy(s));
        return out;
    }

    size_t pos = 0;
    std::string token;
    while ((pos = s.find(delim)) != std::string::npos) {
        token = s.substr(0, pos);
        rd53b::utils::trim(token);
        s.erase(0, pos + delim.length());
        out.push_back(token);
    }
    if (s.length() != 0) out.push_back(s);
    return out;
}

};  // namespace utils
};  // namespace rd53b
