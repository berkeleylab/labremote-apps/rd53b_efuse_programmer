# RD53B E-Fuse Programmer

Utilities for programming the RD53B E-fuse circuitry.

# Table of Contents
  * [Assumptions](#assumptions)
  * [Requirements](#requirements)
  * [Installation](#installation)
  * [Writing the E-fuses](#writing-the-e-fuses)
  * [Reading the E-fuses](#reading-the-e-fuses)

## Assumptions

The current assumptions for using this package is that the users wishes to program the E-Fuse circuitry for
and RD53B ASIC (or many) **one at a time** and that each chip is located on a single-chip card (SCC). If multiple chips are specified in
the provided YARR connectivity files to the [write-efuses](#writing-the-e-fuses) and [read-efuses](#reading-the-e-fuses) utilities,
the operations will not succeed.

## Requirements

The requirements are the union of those required for [labRemote](https://gitlab.cern.ch/berkeleylab/labRemote) and [YARR](https://gitlab.cern.ch/YARR/YARR).

## Installation
```shell
git clone --recursive https://gitlab.cern.ch/berkeleylab/labremote-apps/rd53b_efuse_programmer.git
cd rd53b_efuse_programmer/
source /opt/rh/devtoolset-7/enable # requires same requirements as YARR
mkdir build
cd build/
cmake3 ..
make -j4
```

## Writing the E-fuses
Upon successful [installation](#installation), an executable named `write-efuses` will be located under `rd53b_efuse_programmer/build/bin`:
```shell
$  ./bin/write-efuses -h
 Usage: ./write-efuses [OPTIONS]

 Options:
     -r     YARR controller configuration file
     -c     YARR connectivity configuration file
     -l     labRemote hardware configuration file
     -n     power supply name that contains the "vdd-efuse" power supply channel
     -e     e-fuse data to write [hex-string]
     -h     show this help message
```

The usual configuration files for `YARR` are required in order to communication with the `RD53B` chips.

The `labRemote` hardware configuration file, at minimum,  must look like the below:
```json
{
    "devices":{
        "PS-NAME":{
            "hw-type":"PS",
            "hw-model":"PS-MODEL",
            "communication":{...}
        }
    },
    "channels":{
        "vdd-efuse":{
            "hw-type": "PS",
            "device": "PS-NAME",
            "channel": <channel-number-as-integer>,
            "program": {
                "voltage":2.5,
                "maxvoltage":2.55,
                "maxcurrent":0.5
            }
        }
    }
}
```
Where the connected power-supply with name `"PS-NAME"` is described. The `write-efuse` executable requires that the power suppply channel
that is driving the E-Fuse circuitry is named `vdd-efuse`, as shown in the example `labRemote` configuration above.

An example of writing the data `0xc0cac01a` to the `RD53B` E-fuses is then:
```shell
./bin/write-efuses -r /path/to/controller/specCfg-rd53b.json -c /path/to/connectivity/example_rd53b_setup.json -l /path/to/labremote_config.json -n foo -e c0cac01a
[EFUSE-PROG] E-fuse write succeeded
[EFUSE-PROG]  -> Requested to write   : 0xc0cac01a
[EFUSE-PROG]  -> E-fuse data readback : 0xc0cac01a
```
The `write-efuses` turns on the `vdd-efuse` power-supply channel, runs the programming routine, and then turns off the `vdd-efuse` power-supply channel.
The whole programming process should take no more than 2 seconds.

After the programming routine runs, and the `vdd-efuse` power-supply channel is turned off, the E-fuse data from the chip is read out and compared
to the value that was requested to be written. In the above example, we can see that the data readback after the programming routine
is equal to the value that we tried to write to the RD53B E-fuse registers.

If the E-Fuse data that is readback is not the same as that requested to be written, the following error message is reported:
```shell
./bin/write-efuses -r /path/to/controller/specCfg-rd53b.json -c /path/to/connectivity/example_rd53b_setup.json -l /path/to/labremote_config.json -n foo -e c0cac01a
[EFUSE-PROG] [ERROR] Failed to write e-fuses
[EFUSE-PROG] [ERROR]  -> Attempted to write   : 0xc0cac01a
[EFUSE-PROG] [ERROR]  -> E-fuse data readback : 0xdeadbeef
```

## Reading the E-fuses
In order to read-back the RD53B E-Fuse registers, the `read-efuses` executable is provided:
```shell
$  ./bin/read-efuses -h
 Usage: ./read-efuses [OPTIONS]

 Options:
     -r     YARR controller configuration file
     -c     YARR connectivity configuration file
     -h     show this help message
```

Reading back the RD53B E-fuse data does not require that the E-fuse circuitry be powered. Therefore, only the usual `YARR` configuration files are required.

An example of reading back the E-fuse data is then:
```shell
./bin/read-efuses -r /path/to/controller/specCfg-rd53b.json -c /path/to/connectivity/example_rd53b_setup.json
[EFUSE-PROG] E-fuse data readback : 0xc0cac01a
```
